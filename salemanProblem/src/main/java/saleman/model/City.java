package saleman.model;

import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;

public class City implements  Displayable {
    private final Point2D location;
    private final Circle circle;

    public City(double x, double y) {
        this.location = new Point2D(x, y);
        this.circle = new Circle(x, y, 5);
        CityManager.getInstance().addCity(this);
    }

    @Override
    public void display(Pane root) {
        root.getChildren().add(this.circle);
    }

    @Override
    public void clear(Pane root) {
        root.getChildren().remove(this.circle);
    }


    protected double getDistanceTo(City city) {
        return this.getLocation().distance(city.getLocation());
    }

    protected Point2D getLocation() {
        return this.location;
    }

    @Override
    public String toString() {
        return "(" + (int) this.location.getX() + "," + (int) this.location.getY() + ")";
    }

}